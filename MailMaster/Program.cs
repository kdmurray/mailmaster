﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

using OpenPop.Mime;


namespace MailMaster
{
    class Program
    {
        static void Main(string[] args)
        {

            GmailClient c = new GmailClient(Global.Username, Global.Password);
            MessageParser mp = new MessageParser();


            Console.WriteLine("Found {0} messages", c.GetMessageCount());

            List<Message> msgs = c.GetMessages();

            Console.WriteLine("Downloaded {0} messages", msgs.Count);

            for (int i = 0; i < msgs.Count; i++)
            {
                Console.WriteLine("Processing message {0}...", i + 1);
                Message m = msgs[i];
                MessageType t = mp.DetermineMessageType(m);

                FreeCycleManager fcm;
                EpubManager epm;
                FileManager fmgr;

                switch (t)
                {
                    case MessageType.FreecycleOffer:
                        fcm = new FreeCycleManager();
                        fcm.ProcessOffer(m);
                        break;
                    case MessageType.FreecycleWanted:
                        fcm = new FreeCycleManager();
                        fcm.ProcessWanted(m);
                        break;
                    case MessageType.FreecycleTaken:
                        fcm = new FreeCycleManager();
                        fcm.ProcessTaken(m);
                        break;
                    case MessageType.EpubDecrypt:
                        epm = new EpubManager();
                        epm.Decrypt(m);
                        break;
                    case MessageType.GetDirectoryListing:
                        fmgr = new FileManager();
                        fmgr.GetDirectoryListing(m);
                        break;
                    case MessageType.GetFile:
                        fmgr = new FileManager();
                        fmgr.GetFile(m);
                        break;
                    case MessageType.Undetermined:
                        Console.WriteLine("    No match found. Type: Undetermined.");
                        break;
                    default:
                        break;
                }

                Console.WriteLine("    Deleting message....");
                c.DeleteMessage(i + 1);

            }

            c.Disconnect();

        }

    }
}
