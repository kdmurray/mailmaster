﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MailMaster
{
    public class FreeCycleConfig
    {
        public List<FreeCycleKeywordList> KeywordLists;

        public FreeCycleConfig()
        {
            KeywordLists = new List<FreeCycleKeywordList>();
        }
    }
}
