﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenPop.Mime;

namespace MailMaster
{
    class MessageParser
    {
        public MessageType DetermineMessageType(Message msg)
        {
            Console.WriteLine("    Determining type for message \"{0}\"...", msg.Headers.Subject);

            if (msg.Headers.From.MailAddress.Address.EndsWith("freecycle.org"))
            {
                if (msg.Headers.Subject.StartsWith("[") && msg.Headers.Subject.Contains("] OFFER"))
                    return MessageType.FreecycleOffer;

                if (msg.Headers.Subject.StartsWith("[") && msg.Headers.Subject.Contains("] WANTED"))
                    return MessageType.FreecycleWanted;

                if (msg.Headers.Subject.StartsWith("[") && msg.Headers.Subject.Contains("] TAKEN"))
                    return MessageType.FreecycleTaken;
            }


            if (msg.Headers.Subject.ToLower() == "get file")
            {
                if (msg.ToMailMessage().Body != "")
                {
                    return MessageType.GetFile;
                }
            }


            if (msg.Headers.Subject.ToLower() == "get directory")
            {
                if (msg.ToMailMessage().Body != "")
                {
                    return MessageType.GetDirectoryListing;
                }
            }


            if (msg.Headers.Subject.ToLower() == "epub decrypt")
            {
                bool hasEpubAttachments = false;

                List<MessagePart> atts = msg.FindAllAttachments();

                foreach (MessagePart mp in atts)
                {
                    if (mp.FileName.ToLower().EndsWith(".epub"))
                    {
                        hasEpubAttachments = true;
                    }
                }

                if (hasEpubAttachments)
                {
                    return MessageType.EpubDecrypt;
                }
            }

            return MessageType.Undetermined;
        }
    }
}
