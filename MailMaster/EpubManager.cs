﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

using OpenPop.Mime;

namespace MailMaster
{
    class EpubManager
    {
        public void Decrypt(Message msg)
        {
            if (!Directory.Exists("input"))
            {
                Directory.CreateDirectory("input");
            }
            if (!Directory.Exists("output"))
            {
                Directory.CreateDirectory("output");
            }

            foreach (MessagePart part in msg.FindAllAttachments())
            {
                FileInfo fi = new FileInfo(@"input\" + msg.Headers.From.Address.ToString() + "---" + part.FileName);
                part.SaveToFile(fi);
            }

            string[] files = Directory.GetFiles(@"input", "*.epub");

            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                DirectoryInfo di = new DirectoryInfo("output");

                Console.WriteLine("    Launching decryption of {0}", fi.Name);

                Process p = Process.Start(new ProcessStartInfo("E:\\DRM\\ineptepubv5.py", "E:\\DRM\\adeptkey.der \"" + fi.FullName + "\" \"" + di.FullName + "\\" + fi.Name + "\""));

                while (!p.HasExited)
                {
                    Thread.Sleep(100);
                }

                string email = fi.Name.Substring(0, fi.Name.IndexOf("---"));

                fi.Delete();

                Console.WriteLine("    Sending decrypted files to {0}...", email);

                GmailClient c = new GmailClient(Global.Username, Global.Password);

                List<string> atts = new List<string>();
                if (File.Exists(di.FullName + "\\" + fi.Name))
                {
                    atts.Add(di.FullName + "\\" + fi.Name);
                }
                else
                {
                    Console.WriteLine("    WARNING: EXPECTED FILE NOT FOUND!");
                }

                c.SendMessage("epubdecrypt@noreply.com", email, "Decrypted Epub", "Please find your file(s) attached", atts);

            }

        }
    }
}
