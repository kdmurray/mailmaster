﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;

using OpenPop.Mime;

namespace MailMaster
{
    class FileManager
    {
        long maximumFileLength = 1024 * 1024 * 10; // Default to 10MB

        public Attachment ConvertFileToAttachment(string file)
        {
            FileInfo fi = new FileInfo(file);

            if (fi.Exists)
            {
                if (fi.Length < maximumFileLength)
                {
                    return new Attachment(file);
                }
                else
                {
                    throw new InvalidDataException("File exceeds the maximum file size of " + maximumFileLength.ToString() + " bytes.");
                }
            }
            else
            {
                throw new FileNotFoundException("File " + file + " not found.");
            }

        }

        public void GetFile(Message msg)
        {
            string file = msg.ToMailMessage().Body;
            string body = "";

            FileInfo fi = new FileInfo(Global.CleanString(file));
            List<string> att = new List<string>();

            try
            {
                if (!fi.Exists)
                {
                    body = "File " + fi.FullName + " not found.";
                }
                if (fi.Length > maximumFileLength)
                {
                    body = "File exceeds the maximum file size of " + maximumFileLength.ToString() + " bytes.";
                }

                att.Add(fi.FullName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            GmailClient c = new GmailClient(Global.Username, Global.Password);
            c.SendMessage("filemanager@noreply.com", msg.Headers.From.Address, fi.Name, body, att);
        }

        public void GetDirectoryListing(Message msg)
        {

            string directory = msg.ToMailMessage().Body;
            directory = Global.CleanString(directory);

            string body = "";

            DirectoryInfo di = new DirectoryInfo(directory);

            StringBuilder directoryListing = new StringBuilder();

            try
            {
                if (!Directory.Exists(directory))
                {
                    body = "Directory " + directory + " not found.";
                }

                directoryListing.Append("DIRECTORY LISTING FOR " + directory + System.Environment.NewLine + System.Environment.NewLine);

                DirectoryInfo[] dirs = di.GetDirectories();
                FileInfo[] files = di.GetFiles();

                foreach (DirectoryInfo d in dirs)
                {
                    directoryListing.Append(d.FullName.PadRight(60));
                    directoryListing.Append(System.Environment.NewLine);
                }

                foreach (FileInfo f in files)
                {
                    directoryListing.Append(f.FullName.PadRight(60));
                    directoryListing.Append(f.Length.ToString().PadLeft(14));
                    directoryListing.Append(f.LastWriteTime.ToString().PadLeft(22));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            GmailClient c = new GmailClient(Global.Username, Global.Password);
            c.SendMessage("filemanager@noreply.com", msg.Headers.From.Address, new DirectoryInfo(directory).Name, body + directoryListing.ToString(), new List<string>());

        }
    }
}
