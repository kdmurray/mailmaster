﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MailMaster
{
    public class FreeCycleKeywordList
    {
        public string Type;
        public List<string> Keywords;
        public List<string> IgnoreWords;

        public FreeCycleKeywordList()
        {
            Type = "";
            Keywords = new List<string>();
            IgnoreWords = new List<string>();
        }
    }
}
