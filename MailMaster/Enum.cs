﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MailMaster
{
    enum MessageType
    {
        Undetermined,
        FreecycleOffer,
        FreecycleWanted,
        FreecycleTaken,
        EpubDecrypt,
        GetFile,
        GetDirectoryListing
    }
}
