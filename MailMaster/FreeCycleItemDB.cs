﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MailMaster
{
    public class FreeCycleItemDB
    {
        public List<FreeCycleItem> ItemList;
        public string ForwardingAddress;

        public FreeCycleItemDB()
        {
            ItemList = new List<FreeCycleItem>();
            ForwardingAddress = "";
        }
    }
}
